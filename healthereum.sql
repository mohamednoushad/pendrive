-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: healthereum
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Correlation`
--

DROP TABLE IF EXISTS `Correlation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Correlation` (
  `userID` varchar(45) NOT NULL,
  `globalID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Correlation`
--

LOCK TABLES `Correlation` WRITE;
/*!40000 ALTER TABLE `Correlation` DISABLE KEYS */;
INSERT INTO `Correlation` VALUES ('12','0dcb33f0303d11e9a0d5094db6046fcf'),('121212121212','bac675f0311a11e9b1ec57ad4c814a46'),('1234','3979f1c05c2111e98245e322d6b18abb'),('124578','daa4c0d05c2011e98f69c17a53e5dfe3'),('13','863250d0304211e9a0d5094db6046fcf'),('131313131313','48ea1850311b11e9b1ec57ad4c814a46'),('14','682edf50305011e9a0d5094db6046fcf'),('141414141414','cc4988c0311b11e9b1ec57ad4c814a46'),('142536','1936432060e311e999ba876850b17d94'),('14741','d89b4b505c1711e98f57a125e0f711ee'),('147514751475','13264d20670711e998f395661030e5ce'),('14789632','178e84905c1711e9a79e3500ec9cd7e8'),('1591','ad046b105b5011e9a5e74b7cbacbb02e'),('1593573','76f144f05c1611e9a79e3500ec9cd7e8'),('16','41da5100305911e9a0d5094db6046fcf'),('565656565656','09b070e04a2e11e9ac7ee9ef043f644f'),('6942','d3db70f05c2511e9b82309e0aecef8f0'),('874874874874','f1da8ce0755611e988afe1d8d847e3c3'),('879','d1b164e04a2511e9ac7ee9ef043f644f'),('98959895','53d002f05c2411e98d6319fa99fc03bc'),('989598951','b67b1b105c2411e98aabcbf858d61932'),('9895989512','0ab574005c2511e9b82309e0aecef8f0'),('994699469946','750612504ba511e9b35e297587e31e55');
/*!40000 ALTER TABLE `Correlation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MultipleUUID`
--

DROP TABLE IF EXISTS `MultipleUUID`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MultipleUUID` (
  `globalID` varchar(50) DEFAULT NULL,
  `emrOrder` int(11) DEFAULT NULL,
  `system` varchar(25) DEFAULT NULL,
  `cipher` varchar(255) NOT NULL DEFAULT '000'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MultipleUUID`
--

LOCK TABLES `MultipleUUID` WRITE;
/*!40000 ALTER TABLE `MultipleUUID` DISABLE KEYS */;
INSERT INTO `MultipleUUID` VALUES ('41da5100305911e9a0d5094db6046fcf',1,'2','0e22e1d6aa5635ce593a5ca910270f0c8d3eabc820497d2d5564ef861aa109b5a7b8b1b6fde813376b3a349d86615bb65e18883f837b77775c6de213ec9cd3f9f8383edd8c1d3fac837ead2ec32c65a5da53d59bb8ebbc66d341e1f218902e4d'),('d1b164e04a2511e9ac7ee9ef043f644f',1,'2','17b842b439790d018ea69b614e92e6cb1420b5178a009d8e99669d8f618f83168180ff14ec1a327dcfecb755c54818cb20b9e765b4b91415ed5bda6f2168a90e462b099bc20f7a07d9a74979115f1bcd968395d3e68961165c8e2246c2082be2'),('bac675f0311a11e9b1ec57ad4c814a46',1,'2','19d954db9def4d0a9eff919925ebeb61c9d89830cc3ae6398a5ecd25704cd6e4afe25136db4ac6a2ae4e348a3c0994998e6a33db7e98c70266a4bd0a5d2bf0b426863aab534dc8df42f11132502968a3bc57d71c06ef845abaf877d5cf5faa6b'),('682edf50305011e9a0d5094db6046fcf',1,'2','1f77e58c2d020eeddbfd1ad50df1e8a400ab55f1c8cbe4639eb192093432efa13bd1c24340b61c740d8989f4be7224b6704a02bd50beb8a492d6726a595a40f9f37f5cbc017c07c98b98c12c5a072124ef14aa1ab498fb79c235453c33e44553'),('863250d0304211e9a0d5094db6046fcf',1,'2','2a9b72d7cbfbe215b8ee5ae3113c41f98c520d521d285ae8f3a2b84338e05de4519c5a94625a6000fc622cd370ee338694a9ce9804495fb86ca2a713d7a80b0db6afb23042e3282b08e2f7af95048770a164f360bbf7817061501e4e3e086ccc'),('1',2,'3','4'),('1',2,'3','5'),('09b070e04a2e11e9ac7ee9ef043f644f',1,'2','5fb21f3a638e725cf751877823675ff00eb4b21756c72ffc7ec145057887c588a7e6f90b0403ab0499c2ccfdf563e44c7544b28aefc5e51e416fab83fb2eb1ea13ae8024599e495ea7376fde94d58d25f029ff8f2d4301b64ed03d1f9b7d9698'),('750612504ba511e9b35e297587e31e55',1,'2','67e09d39f03e5307edd1b6dc9d4bf834e02e41298d4609a897b351046c5f3b5a523d37be7a3c81c3d63ed6cf38c048ef619f4aebe2d66a184c6119fe2acbc020daa3000b6130696221b35c5f72513cbce17d41da8e2acbe9eaa4a8d783049255'),('48ea1850311b11e9b1ec57ad4c814a46',1,'2','80e1b57fdfd5458c911bb9f3bfb6416b137934490c8f096cbd277bae3feca5287f09936b4993eaaa9da2e24c6689c18b23c6fae5822e49acec0a368ecdafd889c6ea7ecbb65c52e4285862775a09881c9bb6e6dfe8abdec71f6554b83f070ddc'),('cc4988c0311b11e9b1ec57ad4c814a46',1,'2','acc57968123be6fcecdde72499938ee8124361782d6369b0266b793afc8459f8865000dd2f4ed8522f0e540f15d9ff0be74b6ba1c1a42818c912f13f62df24525bc1d624c4b0e34201b78ee81a9a4646b343c82569748af43f9c4a32708dec38'),('0dcb33f0303d11e9a0d5094db6046fcf',1,'2','e8dc463c7ba2f72d7d4bf467f8f07e8e5163b5c94fe0c08311cc28880ef5a5059cd61e1c8487cd43effcbb360ae136bd4d24e97234d6dc9017b0a9cc9fa50b9d62f75fdf94e709a234df8fbb8bc0c84101ccc2072b9c3e9aaa51e37f3266c6de'),('ad046b105b5011e9a5e74b7cbacbb02e',1,'2','0e22e1d6aa5635ce593a5ca910270f0c8d3eabc820497d2d5564ef861aa109b5a7b8b1b6fde813376b3a349d86615bb65e18883f837b77775c6de213ec9cd3f9f8383edd8c1d3fac837ead2ec32c65a5da53d59bb8ebbc66d341e1f218902e4d'),('76f144f05c1611e9a79e3500ec9cd7e8',1,'2','ecd64861c80eba6d80996ed5a5c8e025c084ffc49864bd73e1be7848eedb447d569d3289526e306a9d6aa42ac2e11f62f48da3931d5c8b82edef05b181b08f3c3780d5e3ed6aaab25b39bcfca11dcbd3d3d51f739da7c0ba3463927404b41f45'),('178e84905c1711e9a79e3500ec9cd7e8',1,'2','28ae128b9518617559edc4bf28d79b6008f196d477c94043c977bb6c4d951a2d4f02a18d01330a189abffd758e44d5aa82f45399263a95c7d78f9a7629093cc8e9095bf41f944ecf59557e36d913e8e904ad6e41d403cfbdcd53f813c0789d93'),('d89b4b505c1711e98f57a125e0f711ee',1,'2','28ae128b9518617559edc4bf28d79b6008f196d477c94043c977bb6c4d951a2d4f02a18d01330a189abffd758e44d5aa82f45399263a95c7d78f9a7629093cc8e9095bf41f944ecf59557e36d913e8e904ad6e41d403cfbdcd53f813c0789d93'),('3979f1c05c2111e98245e322d6b18abb',1,'2','17b842b439790d018ea69b614e92e6cb3f6756a95e4f2ad74e82093c9aa7e99db331f7b675880e7b312635ea30468062ccb719dbb9282f9f29540ae3b5d8d94c63595e0a8d11eb2be39c8fce7c682cbd3a5999a97d589e02f923448d04506f51'),('53d002f05c2411e98d6319fa99fc03bc',1,'2','2e03abfb396eaf9fa693f9ef9b77cb7fb9208f19b933f6775ddfb40663e10d14c8f7f6b67f5d8c4a876678bc72a4a2aaf364353bc90912c5e94ae8c24ef961303fc9c7ae5d74ae7f92f856daecf8808899fc177119470177035e29fca2ed5904'),('b67b1b105c2411e98aabcbf858d61932',1,'2','2e03abfb396eaf9fa693f9ef9b77cb7f145b28a4883a81f7b5267a35137c205223a30819ae0a283b5df26bd06da1790d5006def1716b1d8a3a36f8a7711447f6769602cde08cf9f33415bf11c7c3a3c54c5829796611a435aa4445ca03680c00'),('0ab574005c2511e9b82309e0aecef8f0',1,'2','2e03abfb396eaf9fa693f9ef9b77cb7f9ae0324b669c701f9992be41c2f286588c72f87a1166c53ad56d300101ab87365f233cd99db9c0696d2272663c6dd8227dc5b83729b72226ee9a5011dfff9192733ab54f085f45f51f1bd7277f0f84e2'),('d3db70f05c2511e9b82309e0aecef8f0',1,'2','0599a61324ee3539de889793c67eef6bbb616d97abc9cff0f0ff12f3ae2a46d80a7bb1fd75e4c01283454cc1cd60842bcfba1abc2342577b12c0b2581dfa3dc8cf528d32a8ff20d3a4a85c500738f3a160207834b1ec137ff9ad8ac767e609ea'),('1936432060e311e999ba876850b17d94',1,'2','ecd64861c80eba6d80996ed5a5c8e025cd4926a053f501478ed8cc31377e77ee8cd3e060b4153dc744da1ec9d3b05af6aa0beb8af174629fbadba7139ce2b2d61a0855599ffc4f5d5dd84ef5ca1ef2b274e73d0102b7c8a4cb54d7c20a29194b'),('13264d20670711e998f395661030e5ce',1,'2','7b72eb82b6ef1e6968d0fd7c1bbbacf6dba80e339054799c2a3689a0dba2913ff71dcad685fe865e55fd304fc3c4d4c97111db4169eda5c9c4ca8fc90314a18aa68bdc49eacfc44869561fd5d585b40ff06903fcd5552cfd85cc4f4938681a75'),('f1da8ce0755611e988afe1d8d847e3c3',1,'2','7b72eb82b6ef1e6968d0fd7c1bbbacf6dba80e339054799c2a3689a0dba2913ff71dcad685fe865e55fd304fc3c4d4c97111db4169eda5c9c4ca8fc90314a18aa68bdc49eacfc44869561fd5d585b40ff06903fcd5552cfd85cc4f4938681a75');
/*!40000 ALTER TABLE `MultipleUUID` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Notifications`
--

DROP TABLE IF EXISTS `Notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Notifications` (
  `Id` varchar(45) NOT NULL,
  `Message` varchar(500) DEFAULT NULL,
  `UserId` varchar(100) DEFAULT NULL,
  `IsRead` int(1) DEFAULT '0',
  `CreatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Notifications`
--

LOCK TABLES `Notifications` WRITE;
/*!40000 ALTER TABLE `Notifications` DISABLE KEYS */;
INSERT INTO `Notifications` VALUES ('10267180-4ba6-11e9-b35e-297587e31e55','undefined has granted ownership to you','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-03-21 12:23:42'),('17d90cf0-30e7-11e9-b1ec-57ad4c814a46','Ronald A Thomas has granted ownership to you','0x2bf980f57b36b10f120a36f3857fb93a4e7a305a',0,'2019-02-15 11:31:11'),('1db87330-7558-11e9-88af-e1d8d847e3c3','undefined has granted ownership to you','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-05-13 13:51:33'),('2bb1a150-3110-11e9-b1ec-57ad4c814a46','undefined has cancelled your ownership','0x2bf980f57b36b10f120a36f3857fb93a4e7a305a',0,'2019-02-15 16:25:13'),('2ec242e0-30f3-11e9-b1ec-57ad4c814a46','undefined has granted ownership to you','0x2bf980f57b36b10f120a36f3857fb93a4e7a305a',0,'2019-02-15 12:57:43'),('3518a450-61d0-11e9-ae3a-5f1a34931cb8','undefined has cancelled your ownership','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-04-18 17:20:48'),('41f67d90-30f3-11e9-b1ec-57ad4c814a46','undefined has cancelled your ownership','0x2bf980f57b36b10f120a36f3857fb93a4e7a305a',0,'2019-02-15 12:58:15'),('511ceca0-7559-11e9-88af-e1d8d847e3c3','undefined has cancelled your ownership','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-05-13 14:00:08'),('53887c50-3127-11e9-b1ec-57ad4c814a46','undefined has granted ownership to you','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-02-15 19:10:59'),('54e402e0-6caa-11e9-a334-55cdaccee286','undefined has granted ownership to you','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-05-02 12:47:24'),('57925da0-4a06-11e9-ac7e-e9ef043f644f','undefined has granted ownership to you','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-03-19 10:47:51'),('5ebb3970-3125-11e9-b1ec-57ad4c814a46','undefined has granted ownership to you','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-02-15 18:56:58'),('60e9a540-30f0-11e9-b1ec-57ad4c814a46','undefined has cancelled your ownership','0x2bf980f57b36b10f120a36f3857fb93a4e7a305a',0,'2019-02-15 12:37:39'),('62618090-753e-11e9-88af-e1d8d847e3c3','undefined has granted ownership to you','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-05-13 10:47:21'),('69808110-305a-11e9-a0d5-094db6046fcf','undefined has cancelled your ownership','0x2bf980f57b36b10f120a36f3857fb93a4e7a305a',0,'2019-02-14 18:44:09'),('6e8a1720-3127-11e9-b1ec-57ad4c814a46','undefined has cancelled your ownership','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-02-15 19:11:44'),('7af5e520-670a-11e9-98f3-95661030e5ce','Teresa D Claude has granted ownership to you','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-04-25 09:00:32'),('7e6d4650-310c-11e9-b1ec-57ad4c814a46','undefined has cancelled your ownership','0x2bf980f57b36b10f120a36f3857fb93a4e7a305a',0,'2019-02-15 15:58:54'),('81a81db0-3056-11e9-a0d5-094db6046fcf','undefined has granted ownership to you','0x2bf980f57b36b10f120a36f3857fb93a4e7a305a',0,'2019-02-14 18:16:11'),('81da4ff0-4a2e-11e9-ac7e-e9ef043f644f','undefined has granted ownership to you','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-03-19 15:35:22'),('82cd3f00-6caa-11e9-a334-55cdaccee286','undefined has cancelled your ownership','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-05-02 12:48:41'),('8dbfd730-6708-11e9-98f3-95661030e5ce','undefined has granted ownership to you','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-04-25 08:46:45'),('91f86680-30ff-11e9-b1ec-57ad4c814a46','undefined has granted ownership to you','0x2bf980f57b36b10f120a36f3857fb93a4e7a305a',0,'2019-02-15 14:26:24'),('9c46b940-310c-11e9-b1ec-57ad4c814a46','undefined has granted ownership to you','0x2bf980f57b36b10f120a36f3857fb93a4e7a305a',0,'2019-02-15 15:59:44'),('a3910e70-6709-11e9-98f3-95661030e5ce','undefined has cancelled your ownership','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-04-25 08:54:31'),('a6c28ef0-4a23-11e9-ac7e-e9ef043f644f','undefined has cancelled your ownership','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-03-19 14:17:39'),('ad01ad30-3125-11e9-b1ec-57ad4c814a46','undefined has cancelled your ownership','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-02-15 18:59:10'),('adfca110-61cf-11e9-ae3a-5f1a34931cb8','undefined has granted ownership to you','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-04-18 17:17:02'),('b1faabb0-30e5-11e9-b1ec-57ad4c814a46','undefined has cancelled your ownership','0x2bf980f57b36b10f120a36f3857fb93a4e7a305a',0,'2019-02-15 11:21:10'),('b58d4650-7557-11e9-88af-e1d8d847e3c3','undefined has cancelled your ownership','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-05-13 13:48:38'),('c5f19a20-30e5-11e9-b1ec-57ad4c814a46','undefined has granted ownership to you','0x2bf980f57b36b10f120a36f3857fb93a4e7a305a',0,'2019-02-15 11:21:44'),('cf408c30-311c-11e9-b1ec-57ad4c814a46','undefined has granted ownership to you','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-02-15 17:55:42'),('d7bb9670-30e5-11e9-b1ec-57ad4c814a46','undefined has cancelled your ownership','0x2bf980f57b36b10f120a36f3857fb93a4e7a305a',0,'2019-02-15 11:22:14'),('e599cf10-30e4-11e9-b1ec-57ad4c814a46','undefined has granted ownership to you','0x2bf980f57b36b10f120a36f3857fb93a4e7a305a',0,'2019-02-15 11:15:27'),('f164b180-4b91-11e9-b35e-297587e31e55','undefined has cancelled your ownership','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-03-21 09:59:40'),('f1fcc270-311c-11e9-b1ec-57ad4c814a46','undefined has cancelled your ownership','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-02-15 17:56:40'),('f26cf760-68a9-11e9-8fee-532e49ad35e7','Teresa D Claude has cancelled your ownership','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-04-27 10:34:34'),('fa0aeb80-51de-11e9-8cc4-4923a4fffc95','undefined has cancelled your ownership','0x456e82debc5ff85cbca6e98e2ff8ce895c0f08ee',0,'2019-03-29 10:26:13');
/*!40000 ALTER TABLE `Notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `providerTable`
--

DROP TABLE IF EXISTS `providerTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `providerTable` (
  `providerNumber` int(11) NOT NULL,
  `providerName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`providerNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `providerTable`
--

LOCK TABLES `providerTable` WRITE;
/*!40000 ALTER TABLE `providerTable` DISABLE KEYS */;
INSERT INTO `providerTable` VALUES (1,'OpenMRS'),(2,'MIMS');
/*!40000 ALTER TABLE `providerTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `globalID` varchar(45) NOT NULL,
  `Account` varchar(255) DEFAULT NULL,
  `otp` varchar(255) DEFAULT NULL,
  `expiry` datetime DEFAULT NULL,
  `userType` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`globalID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('09b070e04a2e11e9ac7ee9ef043f644f','b3f0bda8372d6df98a0cb48e7ad1729d3ce43247','NRKVK3KTNVVWO3DYONDHQR2KGAXUS52K',NULL,NULL,'Tasneem Al Malak'),('0ab574005c2511e9b82309e0aecef8f0','110fe594a2a2091d0f9f9cb9d79bc9b5df66467a','NULL',NULL,NULL,'Neethu Chandran'),('0dcb33f0303d11e9a0d5094db6046fcf','2bf980f57b36b10f120a36f3857fb93a4e7a305a','ONZE2VS2PBATCZTLOJFXS2LZIQZHI3SP',NULL,NULL,'Deborah A Reed'),('13264d20670711e998f395661030e5ce','8dfb78a6c164dca9b425d3cb0fe6ffd98df9562f','IFMEIUBUJI3DO53GJZ3HC3CKNFZXOQLL',NULL,NULL,'Caroline Ross'),('178e84905c1711e9a79e3500ec9cd7e8','b45a4c6624845242f0100fb2ff7916a6db001862','NULL',NULL,NULL,'dummy dummy'),('1936432060e311e999ba876850b17d94','4be7efd1f7dc2b7e3d4f5eb17b91a9a9cf6ab975','OMYDI2DOHBWFGV2PGZHTEYZPJFZUKVKN',NULL,NULL,'Siju Augustin'),('3979f1c05c2111e98245e322d6b18abb','decc2bf4489865b838d776d76ac54bc5d63b67af','NULL',NULL,NULL,'Siju Augustin'),('41da5100305911e9a0d5094db6046fcf','a78b98329ee24a795f3f7704a4609676b8e0dbad','KJYEMQSKINNGSYKPJVZC64RWFNHC6ZLF',NULL,NULL,'Mohamed Noushad'),('48ea1850311b11e9b1ec57ad4c814a46','7cf51a96c4d2bf6d8e0b39064b815e1c8f04a24e','KNBGO22BNFWXKZTONEXXUULKPBTEI2KF',NULL,NULL,'Teresa D Claude'),('53d002f05c2411e98d6319fa99fc03bc','4ad984210e4672b239652af6612727a32c3433a2','NULL',NULL,NULL,'Neethu Chandran'),('682edf50305011e9a0d5094db6046fcf','44e6689ab289bd433666f70a38e48baa51707251','MRSUE4DGO5ZVKWLHOFSEORDVK5YWQ4RW',NULL,NULL,'Jane Gathungu'),('750612504ba511e9b35e297587e31e55','c3c1427b0ccc0708b3cc9c5d3f1bb6107193d0d9','PAXVM42KJVIGSOL2M42C6RSXINKUY2KO',NULL,NULL,'Haleema Al Hakim'),('76f144f05c1611e9a79e3500ec9cd7e8','d0cf7c390e2b151cab267f014fe553903b31f481','NULL',NULL,NULL,'Siju Augustin'),('863250d0304211e9a0d5094db6046fcf','f12e92c7625dad01da1364cfc5dbd9474f348c6e','K5AXM6LEINFTA52LOQ2DMOJLMVRW2SSX',NULL,NULL,'Ronald A Thomas'),('ad046b105b5011e9a5e74b7cbacbb02e','577caa40d0d7799a39aa9520922fd49c46514de2','NULL',NULL,NULL,'Mohamed Noushad'),('b67b1b105c2411e98aabcbf858d61932','8e9149e9aa26991cc927976eea344b2d9edbdcb0','NULL',NULL,NULL,'Neethu Chandran'),('bac675f0311a11e9b1ec57ad4c814a46','456e82debc5ff85cbca6e98e2ff8ce895c0f08ee','JZFWUYTMII2WEQZYNFBWWQSRGE2HA23I',NULL,NULL,'Amy Adams'),('cc4988c0311b11e9b1ec57ad4c814a46','ff57c43a87dedae77dfbd5ea31f05f2d44f82747','JRNEY23ZGRVEMYKIK5KVQVBVGN4UWVTZ',NULL,NULL,'Alexander Y Blake'),('d1b164e04a2511e9ac7ee9ef043f644f','d1a590d0ca40523df7f4dc865756424d8a75e453','GBKDCR3LJM2XQY2SJJ2VQQZVJNLUOUSW',NULL,NULL,'asd asd'),('d3db70f05c2511e9b82309e0aecef8f0','5910789bf96389f0b6a71d54c7d3d522ab2d0d1e','OIVUERRVGFYVCM2FNV5CWRLMGFUUGSBP',NULL,NULL,'Stewart Mascaranheus'),('d89b4b505c1711e98f57a125e0f711ee','8cd680bc627c2160bdb773bb176eeb23b12cf757','NULL',NULL,NULL,'dummy dummy'),('daa4c0d05c2011e98f69c17a53e5dfe3',NULL,'OZVTIZ3ULFUGWMKBO5VVQQ3CIZ4VU5LM',NULL,NULL,'Siju Augustin'),('f1da8ce0755611e988afe1d8d847e3c3','e1629d4398b8d0bc5430b3714cf4b9217241e4e6','GBBE6RDIGRYFILZVHE2WGTK2MZHEQ5LZ',NULL,NULL,'Caroline Ross');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-14 18:02:19
